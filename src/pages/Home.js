import React,{useEffect,useState} from 'react';

import PopularProduct from '../components/PopularProduct/PopularProduct';
import ShopMethod from '../components/ShopMethod/ShopMethod';
import NewProduct from '../components/NewProduct/NewProduct';
import ProductGallery from '../components/Gallery/Gallery';


const Home = (props) => {


   
    return (
        <main>
          <NewProduct />
          <ProductGallery/>
          <PopularProduct />
          <ShopMethod />
        </main>
    );
}

export default Home;
