import React,{useState,useEffect} from 'react'
import PopularProductItem from '../components/PopularProduct/PopularProductItem';
import {useDispatch} from 'react-redux';

import * as cartActions from '../store/action/cartAction';
import * as productActions from '../store/action/productAction';
import SkeletonLoadding from '../components/SkeletonLoadding/skeletonLoadding';

export default function Shop() {

    const [productNumber, setproductNumber] = useState(6);
    const [displayType, setdisplayType] = useState('new');
    const [loadding, setloadding] = useState(true);
    const [productDisplay, setproductDisplay] = useState();
    const dispatch = useDispatch();

    const handleMoreProducts = () =>{
        setproductNumber(productNumber+3);
    }
    const handleAddToCart = (product) =>{
        dispatch(cartActions.addToCart(product));
    }

    const handleFavoriteItem = (product) =>{
        dispatch(productActions.setFavoriteProduct(product));
    }

    const handleProductTypeToDisplay = (type) =>{
        setdisplayType(type);              
    }



    useEffect(() => {
        let isCancel = true;
        const fetchProductByType = async () =>{
            try {
                setloadding(true);
                const response = await fetch(
                    `http://45.32.59.144/api/products?producttype=${displayType}&productnumber=${productNumber}`
                )
                const resData = await response.json();
                if(isCancel){
                    setproductDisplay(resData);
                    setloadding(false);
                }
                } catch (error) {
                    console.log('failed to fetch popular products',error.message);
                }
        }
        fetchProductByType();

        return ()=>isCancel =false;
    }, [displayType,productNumber])





    useEffect(()=>{
        dispatch(productActions.fetchProducts());       
    },[dispatch]);
    
    return (
        <main>
        <section className="popular-items latest-padding">
            <div className="container">
                <div className="row product-btn justify-content-between mb-40">
                    <div className="properties__button">
                        <nav>                                                      
                            <div className="nav nav-tabs" id="nav-tab" role="tablist">
                                <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" 
                                    onClick={()=>handleProductTypeToDisplay('new')}
                                >NewestArrivals</a>

                                <a className="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false" 
                                    onClick={()=>handleProductTypeToDisplay('popular')}
                                > Most populer </a>
                            </div>
                        </nav>
                    </div>
                    <div className="grid-list-view">
                    </div>
                </div>

                <div className="tab-content" id="nav-tabContent">
                    {/* card one */}
                    <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    {loadding?<SkeletonLoadding/>:
                    <div className="row">
                        {
                            productDisplay.data.map((product,index)=>(
                                <PopularProductItem 
                                    product={product}
                                    key={index} 
                                    handleMoreProducts={handleMoreProducts}
                                    handleAddToCart={()=>handleAddToCart(product)}
                                    handleFavoriteItem={()=>handleFavoriteItem(product)}
                                />
                            ))
                        }   
                    </div>
                    }
                    </div>
                    <div className="row justify-content-center">
                            <div className="room-btn pt-70">
                                <button className="btn view-btn1" onClick={handleMoreProducts}>View More Products</button>
                            </div>
                    </div>
                </div>
            </div>    
        </section>
     
    </main>
    )
}
