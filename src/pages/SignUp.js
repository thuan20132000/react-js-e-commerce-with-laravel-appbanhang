import React,{useState,useEffect} from 'react'
import * as authActions from '../store/action/authAction';
import {useSelector,useDispatch} from 'react-redux';
import { useHistory } from "react-router-dom";
import { CircularProgress } from '@material-ui/core';

export default function SignUp(props) {
    const dispatch = useDispatch();
    const history = useHistory();

    const [signupInfo, setsignupInfo] = useState({
        name:'',
        email:'',
        password:''
    });
    const [loadding, setloadding] = useState(false);
    const usersAuth= useSelector(state => state.auth);

    const checkValidEmail = (email) =>{
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }


    const handleSignupSubmit = async (evt) =>{
        setloadding(true);



        await  dispatch(authActions.signup(signupInfo.name,signupInfo.email,signupInfo.password));
        
        if(usersAuth){
            let user = localStorage.getItem('userData');
            history.push('/');
        }else{
            console.log("not logged");
        }

        setloadding(false);
    }
    



    return (
        <section className="login_part section_padding ">
            <div className="container">
            <div className="row align-items-center">
                <div className="col-lg-6 col-md-6">
                <div className="login_part_text text-center">
                    <div className="login_part_text_iner">
                    <h2>New to our Shop?</h2>
                    <p>There are advances being made in science and technology
                        everyday, and a good example of this is the</p>
                    </div>
                </div>
                </div>
                <div className="col-lg-6 col-md-6">
                <div className="login_part_form">
                    <div className="login_part_form_iner">
                    <h3>Welcome Back ! <br />
                        Please Sign in now</h3>
                    <form className="row contact_form">
                        <div className="col-md-12 form-group p_star">
                            <input type="text" className="form-control" id="name" name="name"  placeholder="Username"
                                onChange={(evt)=>setsignupInfo({...signupInfo,name:evt.target.value})}
                            />
                        </div>
                        <div className="col-md-12 form-group p_star">
                            <input type="text" className="form-control" id="email" name="email"  placeholder="Email" 
                                onChange={(evt)=>setsignupInfo({...signupInfo,email:evt.target.value})}
                            />
                        </div>
                        <div className="col-md-12 form-group p_star">
                            <input type="password" className="form-control" id="password" name="password"  placeholder="Password" 
                                onChange={(evt)=>setsignupInfo({...signupInfo,password:evt.target.value})}   
                            />
                        </div>
                        <div className="col-md-12 form-group">
                       
                        <button type="button"  className="btn_3"
                            onClick={handleSignupSubmit}
                        >
                            {loadding?<CircularProgress color="secondary"/>:'Singup'}
                        </button>
                        </div>
                    </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </section>
    )
}
