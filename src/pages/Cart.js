import React,{useState,useEffect, useRef} from 'react';
import CartItem from '../components/Cart/CartItem';
import {useSelector,useDispatch} from 'react-redux';
import * as cartActions from '../store/action/cartAction';
import CheckoutCard from '../components/Checkout/CheckoutCard';
import SelectOption from '../components/Cart/SelectOption';
import SelectOptionAddress from '../components/Cart/SelectOption';


const Cart = () => {
    
    const cart = useSelector(state => state.carts);
    const dispatch = useDispatch();
    const [subTotalPrice, setsubTotalPrice] = useState(0);
    const [loaded, setloaded] = useState(false);
    const [productCart, setproductCart] = useState([]);


    const handleIncreaseProductQuantity = (product) =>{
          dispatch(cartActions.increaseProductQuantity(product));
          handleCaculateTotalPrice();
    }
    const handleDecreaseProductQuantity = (product) =>{
        dispatch(cartActions.decreaseProductQuantity(product));
        handleCaculateTotalPrice();
    }
    const handleDeleteProductCart = (product) =>{
        dispatch(cartActions.removeFromCart(product));
        handleCaculateTotalPrice();
    }
   

    function handleCaculateTotalPrice(){
        let totalPrice = cart.totalAmount;
        console.log(totalPrice);
        setsubTotalPrice(totalPrice);
    }

    


   
    useEffect(() => {
        setsubTotalPrice(cart.totalAmount);
        setproductCart(cart.itemsCart);
    }, [cart.totalAmount,cart])

     
    return (
                    <main>
                    <section className="cart_area section_padding">
                            <div className="container">
                            <div className="cart_inner">
                                <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Product</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                        {
                                            productCart.map((cart,index)=>(
                                                <CartItem 
                                                    cartItem={cart} 
                                                    key={index} 
                                                
                                                    onIncreaseProductQuantity={()=>handleIncreaseProductQuantity(cart)}
                                                    onDecreaseProductQuantity={()=>handleDecreaseProductQuantity(cart)}
                                                    onDeleteProductCart = {()=>handleDeleteProductCart(cart)}
                                                />
                                            ))
                                        }

                                        <tr>
                                        <td />
                                        <td />
                                        <td>
                                        <h5>Subtotal</h5>
                                        </td>
                                        <td>
                                        <h5>${subTotalPrice}</h5>
                                        </td>
                                        </tr>
                                        <tr className="shipping_area">
                                        <td />
                                        <td />
                                        <td>
                                        <h5>Shipping</h5>
                                        </td>
                                        <td>
                                        <div className="shipping_box">
                                            <ul className="list">
                                            <li>
                                                Flat Rate: $5.00
                                                <input type="checkbox"  aria-label="Radio button for following text input" />
                                            </li>
                                            <li>
                                                Free Shipping
                                                <input type="radio" aria-label="Radio button for following text input" />
                                            </li>
                                            <li>
                                                Flat Rate: $10.00
                                                <input type="radio" aria-label="Radio button for following text input" />
                                            </li>
                                            <li className="active">
                                                Local Delivery: $2.00
                                                <input type="radio" aria-label="Radio button for following text input" />
                                            </li>
                                            </ul>
                                            <h6>
                                            Calculate Shipping
                                            <i className="fa fa-caret-down" aria-hidden="true" />
                                            </h6>

                                            <SelectOptionAddress  />
                                                                     
                                           
                                            <input className="post_code" type="text" placeholder="Postcode/Zipcode" />
                                        </div>
                                        </td>
                                        </tr>
                                        </tbody>
                                </table>
                                <div>
                                        <CheckoutCard totalAmount={subTotalPrice}   />
                                </div>
                                </div>
                            </div>
                            </div>
                    </section>
                    </main>

    );
}

export default Cart;
