import React,{useEffect, useState} from 'react';
import { useParams } from 'react-router-dom';
import Skeleton from '@material-ui/lab/Skeleton';

import * as cartActions from '../store/action/cartAction';
import {useSelector,useDispatch} from 'react-redux';

const ProductDetail = (props) => {

    // console.log(match);
    // let product = props;
    let {id} = useParams();
    const dispatch = useDispatch();
    
    const [loadding,setLoadding] = useState(true);
    const [product, setproduct] = useState('');

    useEffect(() => {

        const fetchProductDetail = async () =>{
            setLoadding(true);
            let result = await fetch(`http://45.32.59.144/api/products/${id}`);
            let data = await result.json();
            setproduct(data.data);
            setLoadding(false);
        }
        fetchProductDetail();


    },[]);


    const handleAddToCart = () =>{
        dispatch(cartActions.addToCart(product));
    }
    const SkeletonProduct = () =>{
        return (
            <div>
                    <Skeleton variant="rect" width={210} height={118} />
                    <Skeleton variant="text" />
            </div>
        )
    }
   
    return (
            <main>
                <div className="product_image_area">
                    <div className="container">
                        <div className="row ">
                                <div className="col-lg-12">
                                    <div className="product_img" style={{width:'250px',height:'400px',margin:'auto'}}>
                                    <img src={product.image} alt="#" className="img-fluid w-100"/>
                                    </div>
                                    <div className="col-lg-8" style={{margin:'auto'}}>
                                        <div className="single_product_text text-center">
                                            <h3>{product.name}</h3>
                                            <p style={{color:'red'}}>${product.price}</p>
                                            <p>
                                                {product.description}
                                            </p>
                                            <div className="card_area">
                                                
                                                <div className="add_to_cart">
                                                    <button className="btn btn-success"
                                                        onClick={()=>handleAddToCart()}
                                                    >
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                        </div>
                    </div>
                </div>
         
            <section className="subscribe_part section_padding">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-8">
                            <div className="subscribe_part_content">
                                <h2>Get promotions  updates!</h2>
                                <p>Seamlessly empower fully researched growth strategies and interoperable internal or “organic” sources credibly innovate granular internal .</p>
                                <div className="subscribe_form">
                                    <input type="email" placeholder="Enter your mail"/>
                                    <a href="#" className="btn_1">Subscribe</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    );
}

export default ProductDetail;
