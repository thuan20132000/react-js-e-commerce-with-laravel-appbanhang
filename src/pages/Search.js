import React,{useEffect,useState} from 'react';
import { useParams } from 'react-router-dom';
import SkeletonLoadding from '../components/SkeletonLoadding/skeletonLoadding';
import PopularProductItem from '../components/PopularProduct/PopularProductItem';
import {useDispatch} from 'react-redux';
import * as cartActions from '../store/action/cartAction';
import * as productActions from '../store/action/productAction';

const Search = () => {

    const dispatch = useDispatch();
    let {search} = useParams();
    const [searchProducts, setsearchProducts] = useState();
    const [loadding, setloadding] = useState(true);
    const [productNumber, setproductNumber] = useState(3);

    const handleMoreProducts = () =>{
        setproductNumber(productNumber+3);
    }
    const handleAddToCart = (product) =>{
        dispatch(cartActions.addToCart(product));
    }

    const handleFavoriteItem = (product) =>{
        dispatch(productActions.setFavoriteProduct(product));
    }


    useEffect(() => {
        let isCancel = true;
        const fetchSearch = async () =>{
            try {
                setloadding(true);
                const response = await fetch(
                    `http://45.32.59.144/api/search?q=${search}&numberproduct=${productNumber}`
                )
                const resData = await response.json();
                if(isCancel){
                    setsearchProducts(resData);
                    setloadding(false);
                }
                
            } catch (error) {
                console.log('failed to fetch search',error.message);
            }


        }
        fetchSearch(); 
        return ()=>{
            isCancel = false;
        }
        
    },[search,productNumber])
   
    return (
        <div>
            <div className="popular-items section-padding30">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-xl-7 col-lg-8 col-md-10">
                            <div className="section-tittle mb-70 text-center">
                                <h2>Search Items</h2>
                            </div>
                        </div>
                    </div>
                    {loadding? <SkeletonLoadding/>:
                    <div className="row">
                        {
                        
                            searchProducts.data.map((product,index)=>(
                                <PopularProductItem 
                                    product={product}
                                    key={index} 
                                    handleMoreProducts={handleMoreProducts}
                                    handleAddToCart={handleAddToCart}
                                    handleFavoriteItem={handleFavoriteItem}
                                />
                            ))
                        }    
                    </div>
                    }
                    <div className="row justify-content-center">
                        <div className="room-btn pt-70">
                            <button className="btn view-btn1" onClick={handleMoreProducts}>View More Products</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Search;
