

class Product {
    constructor(id,name,image,description,price,stock,favorite,view){
        this.id = id;
        this.name = name;
        this.image = image;
        this.description = description;
        this.price = price;
        this.stock = stock;
        this.view = view;
        this.favorite = favorite;
    }
}
export default Product;