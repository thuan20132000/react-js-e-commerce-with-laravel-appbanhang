
class Cart{
    constructor(id,name,image,quantity,price,total){
        this.id = id;
        this.name = name;
        this.image = image;
        this.quantity = quantity;
        this.price = price;
        this.total = total;
    }
}
export default Cart;