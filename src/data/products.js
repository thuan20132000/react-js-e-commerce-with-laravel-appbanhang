import Product from '../models/product';

const PRODUCTS= [
    {
        id:1,
        name:'product 1',
        image:'',
        description:'Product Description',
        price:400,
        inventory:10
    },
    {
        id:2,
        name:'product 2',
        image:'',
        description:'Product Description',
        price:400,
        inventory:10
    },
    {
        id:3,
        name:'product 3',
        image:'',
        description:'Product Description',
        price:400,
        inventory:10
    },
    {
        id:4,
        name:'product 4',
        image:'',
        description:'Product Description',
        price:400,
        inventory:10
    },
    {
        id:5,
        name:'product 5',
        image:'',
        description:'Product Description',
        price:400,
        inventory:10
    },
    {
        id:6,
        name:'product 6',
        image:'',
        description:'Product Description',
        price:600,
        inventory:10
    }
]

export default PRODUCTS;