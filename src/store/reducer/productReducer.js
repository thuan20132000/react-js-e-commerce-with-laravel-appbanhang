import {SET_PRODUCT, SET_FAVORITE_PRODUCT} from '../action/productAction';

const initialState = {
    availableProducts:[],

}

export default (state = initialState,action) =>{

    switch(action.type){
        case SET_PRODUCT:
            return{
                availableProducts:action.products
            }
        case SET_FAVORITE_PRODUCT:
            let favProduct = action.product;
            
            const tempProduct = state.availableProducts;
            const newProduct = tempProduct.findIndex((prod,index)=>{
                if(prod.id === favProduct.id){
                    prod.favorite = true;
                }
            })            
            return{...state,newProduct}

    
        
        default : break;
    }
    return state;

}