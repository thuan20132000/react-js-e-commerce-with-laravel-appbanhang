import {
    ADD_TO_CART,
    INCREASE_PRODUCT_QUANTITY,
    DECREASE_PRODUCT_QUANTITY,
    REMOVE_FROM_CART,
    DESTROY_CART

} from '../action/cartAction';
import Cart from '../../models/cart';

const initialState = {
    itemsCart:[],
    totalAmount:0,
}

export default (state = initialState,action) =>{

    switch(action.type){
        case ADD_TO_CART:
            const addedProduct = action.product;

            
            let productId = addedProduct.id;
            let productPrice = Number(addedProduct.totalPrice);
            let productQuantity = 1;
            let productName = addedProduct.name;
            let productImage = addedProduct.image;
            let totalPrice = productPrice * productQuantity;

            const checkExist = state.itemsCart.findIndex((prod)=>
                prod.id === addedProduct.id 
            );
            if(checkExist === -1){
                const itemTemp = state.itemsCart;
                const newItem = new Cart(productId,productName,productImage,productQuantity,productPrice,totalPrice);
                itemTemp.push(newItem);
                state.totalAmount += productPrice;
                return {
                    ...state,itemsCart:itemTemp,
                }
            }else{
                return {...state};
            }
            
        
        case INCREASE_PRODUCT_QUANTITY:
            const prodIncrease = action.product;

         
            state.itemsCart.findIndex((prod,index)=>{

                if(prod.id === prodIncrease.id){
                    state.itemsCart[index].quantity++;
                    state.itemsCart[index].total = state.itemsCart[index].quantity * state.itemsCart[index].price;
                    state.totalAmount += state.itemsCart[index].price;
                }
            });
            break;

        case DECREASE_PRODUCT_QUANTITY:
            const prodDecrease = action.product;
            state.itemsCart.findIndex((prod,index)=>{
                if(prod.id === prodDecrease.id){
                    state.itemsCart[index].quantity--;
                    state.itemsCart[index].total -= state.itemsCart[index].price;
                    state.totalAmount -= state.itemsCart[index].price;
                }
            });
            break;


        case REMOVE_FROM_CART:
            const productRemove = action.product;
            const itemTemp = state.itemsCart;
            itemTemp.findIndex((prod,index)=>{
                if(prod.id === productRemove.id){
                    state.totalAmount -= itemTemp[index].total;
                    itemTemp.splice(index,1);
                    return itemTemp;
                }
            });

            return {...state,itemsCart:itemTemp};
        
        case DESTROY_CART:
            console.log('destroy cart reducer');
                state.itemsCart = [];
                state.totalAmount = 0;

            return {itemsCart:[],totalAmount:0};
            
        default : break;
    }

    
    return state;

}