export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const DESTROY_CART = 'DESTROY_CART';
export const ADD_ORDER = 'ADD_ORDER';
export const INCREASE_PRODUCT_QUANTITY = 'INCREASE_PRODUCT_QUANTITY';
export const DECREASE_PRODUCT_QUANTITY = 'DECREASE_PRODUCT_QUANTITY';

export const addToCart = (product) =>{
    return {
        type:ADD_TO_CART,
        product:product
    }
}

export const removeFromCart = (product) =>{

    return{
        type:REMOVE_FROM_CART,
        product:product
    }
}


export const increaseProductQuantity = (product) =>{
 
    return {
        type:INCREASE_PRODUCT_QUANTITY,
        product:product
    }
}

export const decreaseProductQuantity = (product) =>{

    return {
        type:DECREASE_PRODUCT_QUANTITY,
        product:product
    }
}

export const destroyCart = () =>{
    console.log('destroy carts');
    return {
        type:DESTROY_CART
    }
}
