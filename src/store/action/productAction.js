
import Product from '../../models/product';




export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const SET_PRODUCT = 'SET_PRODUCT';
export const SET_FAVORITE_PRODUCT = 'SET_FAVORITE_PRODUCT';


export const fetchProducts  = () =>{
    return async (dispatch) =>{
        
        const response = await fetch(
            'http://45.32.59.144/api/products'
        );
        const resData = await response.json();

        const loadedProducts = [];

        resData.data.map((product,index)=>{
            loadedProducts.push(new Product(
                product.id,
                product.name,
                product.image,
                product.description,
                product.totalPrice,
                10,
                false,
                1
                )
            )
        })
        dispatch({
            type: SET_PRODUCT,
            products: loadedProducts
        })


       
    }
}


export const setFavoriteProduct = (product) =>{

        console.log('favorite product');
        return {
            type:SET_FAVORITE_PRODUCT,
            product:product
        }

}

