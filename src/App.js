import React from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header/Header';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';

import Footer from './components/Footer/Footer';
import ProductDetail from './pages/ProductDetail';
import Cart from './pages/Cart';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  useParams,
} from "react-router-dom";
import Shop from './pages/Shop';
import Search from './pages/Search';
import SignUp from './pages/SignUp';
import UserProfile from './pages/UserProfile';
import SignIn from './pages/SignIn';


const routes = [
  {
    path:'/',
    component:<Home/>,
    exact:'exact'
    
  },
  {
    path:'/shop',
    component:<Shop />,
  },
  {
    path:'/about',
    component:<About/>
  },
  {
    path:'/contact',
    component:<Contact/>
  },
  {
    path:'/product-detail/:id',
    component:<ProductDetail/>
  },
  {
    path:'/cart',
    component:<Cart/>
  },
  {
    path:'/search/:search',
    component:<Search/>
  },
  {
    path:'/signup',
    component:<SignUp/>
  },
  {
    path:'/signin',
    component:<SignIn/>
  },
  {
    path:'/userprofile',
    component:<UserProfile/>
  },
 
]

function App(props) {

  return (
      <Router>
        <Header />
        <Switch>
          {
            routes.map((route,index)=>(
              <Route path={route.path} exact key={index} >
                {route.component}
              </Route>
            ))
          }
        </Switch>
        <Footer />
      </Router>
  );
}

export default App;
