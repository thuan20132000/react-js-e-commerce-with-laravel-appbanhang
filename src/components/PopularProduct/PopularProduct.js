import React, { useEffect,useState } from 'react';

import {useDispatch} from 'react-redux';

import * as cartActions from '../../store/action/cartAction';
import * as productActions from '../../store/action/productAction';
import PopularProductItem from './PopularProductItem';
import SkeletonLoadding from '../SkeletonLoadding/skeletonLoadding';

const PopularItem = (props) => {

    const [productNumber, setproductNumber] = useState(6);
    const [popularProducts,setpopularProducts] = useState([]);
    const [loadding,setLoadding] = useState(true);
    const dispatch = useDispatch();

    const handleMoreProducts = () =>{
        setproductNumber(productNumber+3);
    }
    const handleAddToCart = (product) =>{
        dispatch(cartActions.addToCart(product));
    }

    const handleFavoriteItem = (product) =>{
        dispatch(productActions.setFavoriteProduct(product));
    }

    useEffect(()=>{
        let isCancel = true;
        const fetchPopularProducts = async () =>{

            try {
            setLoadding(true);
            const response = await fetch(
                `http://45.32.59.144/api/products?producttype=popular&productnumber=${productNumber}`
            )
            const resData = await response.json();
            if(isCancel){
                setpopularProducts(resData);
                setLoadding(false);
            }
            } catch (error) {
                console.log('failed to fetch popular products',error.message);
            }
            
        }
        fetchPopularProducts();

        return ()=>{
            isCancel = false;
        }

    },[productNumber]);

   

    return (
        <div className="popular-items section-padding30">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-xl-7 col-lg-8 col-md-10">
                        <div className="section-tittle mb-70 text-center">
                            <h2>Popular Items</h2>
                            <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                        </div>
                    </div>
                </div>
                {loadding? <SkeletonLoadding/>:
                <div className="row">
                    {
                    
                        popularProducts.data.map((product,index)=>(
                            <PopularProductItem 
                                product={product}
                                key={index} 
                                handleMoreProducts={handleMoreProducts}
                                handleAddToCart={handleAddToCart}
                                handleFavoriteItem={handleFavoriteItem}
                            />
                        ))
                    }    
                </div>
                }
                <div className="row justify-content-center">
                    <div className="room-btn pt-70">
                        <button className="btn view-btn1" onClick={handleMoreProducts}>View More Products</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PopularItem;
