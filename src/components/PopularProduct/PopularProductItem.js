import React from 'react';
import {Link} from 'react-router-dom';


const PopularProductItem = (props) => {
    const {product,handleAddToCart,handleFavoriteItem} = props;
    return (
        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-6" >
            <div className="single-popular-items mb-50 text-center">
                <div className="popular-img">
                    <img src={product.image} alt=""/>
                    <div className="img-cap">
                        <span
                            onClick={()=>handleAddToCart(product)}
                        >
                            Add to cart
                        </span>
                    </div>
                    <div className="favorit-items">
                        <span className="flaticon-heart" onClick={()=>handleFavoriteItem(product)}>
                        
                        </span>
                    </div>
                </div>
                <div className="popular-caption">
                    <h3>
                        <Link 
                            to={'/product-detail/'+product.id}
                        >{product.name}</Link>
                    </h3>
                    <span>${product.totalPrice}</span>
                </div>
            </div>
        </div>
    );
}

export default PopularProductItem;
