import React,{useEffect,useState} from 'react';
import { useSelector } from 'react-redux';
import SkeletonLoadding from '../SkeletonLoadding/skeletonLoadding';
import NewProductItem from './NewProductItem';


const NewProduct = (props) => {

    // const newProducts = useSelector(state => state.products.availableProducts);
    const [newProducts, setnewProducts] = useState([]);
    const [productNumber, setproductNumber] = useState(3);
    const [loadding, setLoadding] = useState(true);
    
    // console.log(props);
    useEffect(() => {

        let isCancel = true;
        const fetchPopularProducts = async () =>{
            setLoadding(true);
            const response = await fetch(
                `http://45.32.59.144/api/products?producttype=new&productnumber=${productNumber}`
            )
            const resData = await response.json();
                if(isCancel){
                    setnewProducts(resData.data);
                    setLoadding(false);
                }           
        }
        fetchPopularProducts();

        return ()=>{
            isCancel = false;
        }

    }, [productNumber])

    return (
        

        <section className="new-product-area section-padding30">
            <div className="container">
                <div className="row">
                    <div className="col-xl-12">
                        <div className="section-tittle mb-70">
                            <h2>New Arrivals</h2>
                        </div>
                    </div>
                </div>
                {loadding?<SkeletonLoadding/>:
                <div className="row">
                    {
                        newProducts.map((product,index)=>(
                           <NewProductItem product={product} key={index} />
                        ))
                    }
                </div>
                }
            </div>
        </section>
    );
}

export default NewProduct;
