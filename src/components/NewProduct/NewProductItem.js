import React from 'react';
import {Link} from 'react-router-dom';

const NewProductItem = (props) => {
    const {product} = props;
    return (
        <div className="col-xl-4 col-lg-4 col-md-6 col-sm-6" >
            <div className="single-new-pro mb-30 text-center">
                <div className="product-img">
                    <img src={product.image} alt=""/>
                </div>
                <div className="product-caption">
                    <h3>
                        <Link 
                            to={'/product-detail/'+product.id}
                        >{product.name}</Link>
                    </h3>
                    <span>{`$${product.totalPrice}`}</span>
                </div>
            </div>
        </div>
    );
}

export default NewProductItem;
