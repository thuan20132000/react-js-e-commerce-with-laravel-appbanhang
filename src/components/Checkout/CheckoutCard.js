
import React from 'react'
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import CheckoutForm from './CheckoutForm';

// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe('pk_test_51GuJdoFBmCeaY7TFnNrRtgvgyrCJWJBAdOOiNMkXEdBSO62WlSzVvtpYhWd70gFa7RmuxhHEzW1hsc2bayZ85Xhu00Vhn30plg');

const CheckoutCard = (props) => {
  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm {...props} />
    </Elements>
  );
};

export default CheckoutCard;