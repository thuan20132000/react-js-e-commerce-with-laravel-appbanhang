
import React,{useState,useEffect} from 'react';
import {CardElement, useStripe, useElements} from '@stripe/react-stripe-js';
import { CircularProgress } from '@material-ui/core';
import {useDispatch,useSelector} from 'react-redux';

import '../StyleCss/styles.css';
import * as cartActions from '../../store/action/cartAction';


const CheckoutForm = (props) => {
  const stripe = useStripe();
  const elements = useElements();
  const dispatch = useDispatch();
  const [error, setError] = useState(null);
  const [processing, setProcessing] = useState(false);
  const [isErrorPayment, setisErrorPayment] = useState({
      isError : null,
      message:""
  });
  const {totalAmount} = props;
  const [billingDetails, setBillingDetails] = useState({
    email: '',
    phone: '',
    name: '',
  });
  const carts = useSelector(state => state.carts);

  useEffect(() => {
    
    console.log(carts);

  }, [carts,processing])
  


  const checkValidation = () =>{
      if(!billingDetails.name || !billingDetails.phone || !billingDetails.email){
        setError('Please Enter Your Information for Charging!!');
        return false;
      }
      setError(null);
      return true;
  }

  const handleSubmit = async  (event) => {
    setProcessing(true);
    event.preventDefault();
    if(!checkValidation()){
      setProcessing(false);
      return;
    }
    dispatch(cartActions.destroyCart());



      if (!stripe || !elements) {
        // Stripe.js has not loaded yet. Make sure to disable
        // form submission until Stripe.js has loaded.
        return;
      }

      event.preventDefault();

      if (!stripe || !elements) {
        // Stripe.js has not loaded yet. Make sure to disable
        // form submission until Stripe.js has loaded.
        return;
      }
  
      // Get a reference to a mounted CardElement. Elements knows how
      // to find your CardElement because there can only ever be one of
      // each type of element.
      const cardElement = elements.getElement(CardElement);
  
      // Use your card Element with other Stripe.js APIs
      const {error, paymentMethod} = await stripe.createPaymentMethod({
        type: 'card',
        card: cardElement,
        billing_details:billingDetails
      });
  
      if (error) {
        console.log('[error]', error);
        setProcessing(false);
      } else {
        console.log('[PaymentMethod]', paymentMethod);

        const sendPayment = await fetch(
              'http://45.32.59.144/api/checkout',
              {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
              },
                body: JSON.stringify({
                    paymentMethod:paymentMethod,
                    amount:totalAmount,
                    billingDetails:billingDetails
                })
              }
        );
        const res = await sendPayment.json();
        console.log(billingDetails);
        if(!res.state){
          setisErrorPayment({
            isError:true,
            message:"Payment Failed, Please Check your information and order again!!"
          });
        }else{
          
          setisErrorPayment({
            isError:false,
            message:"Payment Success, Please see more information in your email."
          });
          setBillingDetails({
            name:'',
            email:'',
            phone:''
          });

        }
        console.log(isErrorPayment);

        setProcessing(false);

      }
  

  }

  
  const CARD_OPTIONS = {
    iconStyle: 'solid',
    style: {
      base: {
        iconColor: '#c4f0ff',
        color: '#5f6363',
        fontWeight: 500,
        fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
        fontSize: '16px',
        fontSmoothing: 'antialiased',
        ':-webkit-autofill': {color: '#fce883'},
        '::placeholder': {color: '#87bbfd'},
        padding:'20px'
      },
      invalid: {
        iconColor: 'red',
        color: 'red',
      },
    },
  };

  

  return (
    <div>
          {isErrorPayment.isError !== null && !processing ? <span  style={{color:'white',backgroundColor:isErrorPayment.isError?'red':'blue'}}>{isErrorPayment.message}</span>:""} 
          {error ? <span style={{color:'red'}}>{error}</span>:''}
          <section className="form-checkout">
            <div className="row">
              <div className="col-md-12 col-lg-12">
                <label className="primary">
                  Name
                </label>
                <input
                  type={'text'}
                  placeholder={'name'}
                  required
                  value={billingDetails.name}
                  onChange={(evt)=>setBillingDetails({...billingDetails,name:evt.target.value})}
                />
              </div>
              <div className="col-md-12 col-lg-12">
                <label className="primary">
                  Phone 
                </label>
                <input
                  type='text'
                  placeholder={'phone number'}
                  required
                  value={billingDetails.phone}
                  onChange={(evt)=>setBillingDetails({...billingDetails,phone:evt.target.value})}
                />
              </div>
              <div className="col-md-12 col-lg-12">
                <label className="primary">
                  Email 
                </label>
                <input
                  type='text'
                  placeholder={'email'}
                  required
                  value={billingDetails.email}
                  onChange={(evt)=>setBillingDetails({...billingDetails,email:evt.target.value})}
                />
              </div>
            </div>
          </section>

        <CardElement options={CARD_OPTIONS} />
        <button type="submit" onClick={handleSubmit}  className='btn btn-primary btn-block mt-4 __name' style={{backgroundColor:'#2677fc'}}>
          {processing?<CircularProgress/>:'Pay'}
        </button>
    </div>
      
  );
};

export default CheckoutForm;