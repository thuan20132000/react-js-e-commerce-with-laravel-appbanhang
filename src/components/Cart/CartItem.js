import React,{useState,useEffect} from 'react';

const CartItem = (props) => {
    const {cartItem,onDeleteProductCart,onIncreaseProductQuantity,onDecreaseProductQuantity} = props;
    
    const [productNumber, setproductNumber] = useState(1);
    
    const increaseProductQuantity = () =>{
        setproductNumber(productNumber+1);
        onIncreaseProductQuantity();
    }
    const decreaseProductQuantity = () =>{
        if(productNumber <=1){
            return;
        }
        onDecreaseProductQuantity();
        setproductNumber(productNumber-1);
        
    }
    const changeProductNumber = (evt) =>{
        let productNum = evt.target.value;
        setproductNumber(productNum);
    }

    useEffect(() => {
        setproductNumber(cartItem.quantity)
    }, [cartItem.quantity])




    return (
        <tr>
            <td>
            <div className="media">
                <div className="d-flex">
                <img src={cartItem.image} alt="" />
                </div>
                <div className="media-body">
                <p>{cartItem.name}</p>
                </div>
            </div>
            </td>
            <td>
            <h5>${cartItem.price}</h5>
            </td>
            <td>
            <div className="product_count">
                <span className="input-number-decrement" onClick={decreaseProductQuantity}> <i className="ti-minus" /></span>
                <input 
                    className="input-number" 
                    type="number" 
                    value={productNumber}
                    readOnly
                
                />
                <span className="input-number-increment" onClick={increaseProductQuantity}> <i className="ti-plus" /></span>
            </div>
            </td>
            <td>
            <h5>${productNumber * cartItem.price}</h5>
            
            </td>
            <td>
            <button 
                className="btn btn-danger"
                onClick={onDeleteProductCart}
            >Delete</button>
            </td>
        </tr>
    );
}

export default CartItem;
