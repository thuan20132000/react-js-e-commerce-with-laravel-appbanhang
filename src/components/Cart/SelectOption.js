import React,{useEffect,useState} from 'react'

export default function SelectOptionAddress(props) {

    const {label} = props;

    const [provinceArr, setprovinceArr] = useState([]);
    const [districtArr, setdistrictArr] = useState([]);
    const [subDistrictArr, setsubDistrictArr] = useState([]);
    const [provinceId, setprovinceId] = useState('01');
    const [districtId, setdistrictId] = useState('');
    const [subDistrictId, setsubDistrictId] = useState('');

    const [loadding, setloadding] = useState(false);
    useEffect(() => {
        const fetchProvinces = async () =>{
            setloadding(true);
            const result = await fetch(
                `https://raw.githubusercontent.com/madnh/hanhchinhvn/master/dist/tinh_tp.json`
            );
            const res = await result.json();          
            setprovinceArr(res);
            setloadding(false);

        }
        fetchProvinces();
    }, []);

    const handleChaneProvince = (evt) =>{
        let provId = evt.target.value;
        setprovinceId(provId);
        setsubDistrictArr(['']);
   
    }
    useEffect(() => {
        const fetchDistricts = async () =>{

            const result = await fetch(
                `https://raw.githubusercontent.com/madnh/hanhchinhvn/master/dist/quan-huyen/${provinceId}.json`
            );
            const res = await result.json();
            setdistrictArr(res);
        }
        fetchDistricts();
    }, [provinceId])



    const handleChangeDistrict = (evt) =>{
        let districtId = evt.target.value;
        setdistrictId(districtId);
    }
    useEffect(() => {
        const fetchSubDistrict = async () =>{

            const result = await fetch(
                `https://raw.githubusercontent.com/madnh/hanhchinhvn/master/dist/xa-phuong/${districtId}.json`
            );
            const res = await result.json();
            setsubDistrictArr(res);
        }
        if(districtId){
            fetchSubDistrict();
        }
    }, [districtId])
    

    const handleSubDistrict = (evt) =>{
        let subDistrictId = evt.target.value;
        setsubDistrictId(subDistrictId);

        console.log('province: ',provinceId);
        console.log('district: ',districtId);
        console.log('sub district: ',subDistrictId);

    }
   
    

        return (
            <>
            <select className="shipping_select section_bg" onChange={handleChaneProvince}>
                <option>Select Province</option>
                {
                  !loadding &&  Object.values(provinceArr).map((prov,index)=><option key={prov.code+1} value={prov.code}>{prov.name}</option>)
                }   
            </select>

            <select className="shipping_select section_bg" onChange={handleChangeDistrict}>
                <option>Select District</option>
                {
                   Object.values(districtArr).map((prov,index)=><option key={prov.code+1} value={prov.code}>{prov.name}</option>)
                }  
            </select>
            <select className="shipping_select section_bg" onChange={handleSubDistrict}>
                <option>Select Sub-District</option>
                {
                   Object.values(subDistrictArr).map((prov,index)=><option key={prov.code+1} value={prov.code}>{prov.name}</option>)
                }  
            </select>

            </>
        )
   
    
}
