import React,{useState,useEffect} from 'react'
import {
    NavLink,
    Link
} from "react-router-dom";

import {useSelector} from 'react-redux';
  
  

  
const  Header = () => {

    const [cartNumber, setcartNumber] = useState(0);
    const carts = useSelector(state => state.carts);
    const auth = useSelector(state => state.auth);
    const [isLogged, setisLogged] = useState(false);
    const [username, setusername] = useState('');
    const [search, setsearch] = useState('');

    const handleSearch = (evt) =>{
        let search = evt.target.value;
        setsearch(search);
    }

    useEffect(() => {
        setcartNumber(carts.itemsCart.length);
    },[carts])

   useEffect(() => {
        let log = localStorage.getItem('userData');
        let logJson = JSON.parse(log);
        if(logJson){
            setisLogged(true);
            setusername(logJson.name);
        }

   }, [auth]);

   const handleLogout = () =>{
        setisLogged(false);
       localStorage.setItem('userData',null);
   }


    return (
        <header>
            <div className="header-area">
                <div className="main-header header-sticky">
                    <div className="container-fluid">
                        <div className="menu-wrapper">
                            <div className="logo">
                                <a href="index.html"><img src="assets/img/logo/logo.png" alt=""/></a>
                            </div>
                            <div className="main-menu d-none d-lg-block">
                                <nav>                                                
                                    <ul id="navigation">  
                                        <li>
                                            <NavLink  to="/" >Home</NavLink>
                                        </li>
                                        <li>
                                            <NavLink  to="/shop" >Shop</NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="/about" >About</NavLink>
                                        </li>
                                        <li>
                                            <NavLink to="/contact">Contact</NavLink>
                                        </li>
                                        
                                    </ul>
                                </nav>
                            </div>
                            <div className="header-right">
                                <ul>
                                    <li>
                                        <div className="nav-search search-switch">
                                            <input 
                                                type="text" 
                                                placeholder="Enter to search"
                                                onChange={handleSearch}
                                            />
                                            <Link to={'/search/'+search}><span className="flaticon-search"></span></Link>
                                        </div>
                                    </li>
                                    <li>
                     
                                    </li>
                                    <li className="header-cart-link">
                                        <Link to='/cart'>
                                            <span className="flaticon-shopping-cart cart-num-icon">
                                                {cartNumber>0 && <span className="cart-num">{cartNumber}</span> }
                                            </span>
                                        </Link>
                                    </li>
                                    {
                                        !isLogged ?
                                                    <Link to='/signin'>
                                                        <span className="flaticon-signup">
                                                            Login
                                                        </span>
                                                    </Link>
                                            :
                                          
                                                <li className="dropdown">
                                                  <Link className="dropdown-toggle text-danger" to="/signup" id="dropdownMenuLink" data-toggle="dropdown" >
                                                    {username}
                                                  </Link>
                                                  <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                    <Link to="/userprofile" className="dropdown-item text-danger">profile</Link>
                                                    <Link to="/" className="dropdown-item text-danger">infor</Link>
                                                    <Link to="/signup" className="dropdown-item text-danger"
                                                        onClick={handleLogout}
                                                    >Log out</Link>


                                                  </div>
                                                </li>
                                    }
                                    
                                    
                                </ul>
                            </div>
                            
                        </div>
                       
                        <div className="col-12">
                            <div className="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
            
        </header>
    )
}

export default Header;