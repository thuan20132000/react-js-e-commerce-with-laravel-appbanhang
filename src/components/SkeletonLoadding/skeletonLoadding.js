

import React from 'react'
import Skeleton from '@material-ui/lab/Skeleton';

export default function skeletonLoadding() {
    
    const elements = [1,2,3];
 
    return (
        
        
        <div className="container" >
            
            <div className="row">
            {
               elements.map((value, index) => {
                return (
                   <div key={index} className="col-xl-4 col-lg-6 col-md-6">
                        <Skeleton variant="rect" height={300}/>
                   </div>
                )  
              })
            }
           </div>
        </div>

    )
}
